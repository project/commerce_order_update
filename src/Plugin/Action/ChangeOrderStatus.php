<?php

namespace Drupal\commerce_order_update\Plugin\Action;

use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Change Order Status.
 *
 * @Action(
 *   id = "change_order_status",
 *   label = @Translation("Change Order Status"),
 *   type = "commerce_order",
 *   confirm = TRUE,
 * )
 */
class ChangeOrderStatus extends ViewsBulkOperationsActionBase implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'order_state' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $workflow_manager = \Drupal::service('plugin.manager.workflow');

    /** @var \Drupal\commerce_order\Entity\OrderType $defaultOrderType */
    $defaultOrderType = \Drupal::entityTypeManager()->getStorage('commerce_order_type')->load('default');
    
    // The load method returns null if entity does not exist so we check for that
    // here in case the default order type has been deleted.
    if (is_null($defaultOrderType)) {
      $defaultOrderTypeWorkflow = 'order_fulfillment_validation';
    }
    else {
      $defaultOrderTypeWorkflow = $defaultOrderType->getWorkflowId();
    }
    
    $orderFulfillmentValidation = $workflow_manager->createInstance($defaultOrderTypeWorkflow);
    $states = $orderFulfillmentValidation->getStates();

    $order_states = [];
    foreach ($states as $state_id => $state) {
      $order_states[$state_id] = $state->getLabel();
    }

    $form['order_state'] = [
      '#title' => $this->t('Order State'),
      '#type' => 'select',
      '#empty_option' => $this->t('- Select Order State -'),
      '#description' => $this->t("Change order(s) to the state selected above."),
      '#options' => $order_states,
      '#default_value' => !empty($this->configuration['order_state']) ? $this->configuration['order_state'] : '',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['order_state'] = $form_state->getValue('order_state');
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    if (!empty($this->configuration['order_state'])) {
      $entity->set('state', $this->configuration['order_state']);
      $entity->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\commerce_order\Entity\Order $object */
    $result = $object->access('update', $account, TRUE);
    return $return_as_object ? $result : $result->isAllowed();
  }

}
